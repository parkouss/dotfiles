#!/usr/bin/env python3

import os
import abc
import subprocess
import argparse
import tempfile
from configparser import ConfigParser, ExtendedInterpolation
from glob import glob

THIS_DIR = os.path.dirname(os.path.realpath(__file__))


class SystemMeta(abc.ABCMeta):
    known_systems = []

    def __new__(mcls, name, bases, namespace, **kwargs):
        cls = super().__new__(mcls, name, bases, namespace, **kwargs)
        if name != "System":
            assert getattr(cls, "name")
            SystemMeta.known_systems.append(cls)
        return cls

    @classmethod
    def get_current_system(cls):
        for system in cls.known_systems:
            if system.is_current_system():
                return system()


class System(metaclass=SystemMeta):
    @classmethod
    @abc.abstractmethod
    def is_current_system(cls):
        pass

    @classmethod
    @abc.abstractmethod
    def name():
        pass

    @abc.abstractmethod
    def make_package_cmd(self, package):
        pass


class Arch(System):
    name = "arch"

    @classmethod
    def is_current_system(cls):
        return os.path.isfile("/etc/arch-release")

    def make_package_cmd(self, package):
        return f"pacman -Q {package} >/dev/null 2>&1 || pacman -S {package}"


class Fedora(System):
    name = "fedora"

    @classmethod
    def is_current_system(cls):
        return os.path.isfile("/etc/fedora-release")

    def make_package_cmd(self, package):
        return f"rpm -q {package} >/dev/null || dnf install -y {package}"


CURRENT_SYSTEM = SystemMeta.get_current_system()


class MetaPackage:
    def __init__(self, path):
        pkg_dir = os.path.dirname(path)
        parser = ConfigParser(defaults={"PKG_DIR": pkg_dir},
                              interpolation=ExtendedInterpolation())
        parser.read([path])
        self.name = os.path.basename(pkg_dir)
        self.__cfg = parser

    def supported_systems(self):
        return self.__cfg.sections()

    def package_for(self, system):
        return Package(self.name, system, self.__cfg[system.name])


class Package(abc.ABC):
    def __init__(self, name, system, cfg):
        self.name = name
        self.system = system
        self.__cfg = cfg

    def section(self, name):
        return self.__cfg.get(name, "")


def all_meta_packages():
    return [MetaPackage(f)
            for f in glob(os.path.join(THIS_DIR, "*/pkg.ini"))]


def all_packages(meta_packages=None, system=None):
    if system is None:
        system = CURRENT_SYSTEM
    if meta_packages is None:
        meta_packages = all_meta_packages()
    return [m.package_for(system) for m in meta_packages
            if system.name in m.supported_systems()]


def shell_out(script, sudo=False, dry_run=False):
    if dry_run:
        print("[under sudo]" if sudo else "[not under sudo]")
        print(script)
        print()
        return
    with tempfile.NamedTemporaryFile("w") as f:
        f.write(script)
        f.flush()

        command = ["sudo"] if sudo else []
        command.extend(("sh", "-xe", "-", f.name))
        return subprocess.call(command)


def packages_run_section(packages, section, sudo=False, dry_run=False):
    scripts = []
    if section == "install":
        system_packages_cmd = set()
        for p in packages:
            for sp in p.section("packages").strip().split():
                system_packages_cmd.add(p.system.make_package_cmd(sp))
        if system_packages_cmd:
            scripts.append("")
            scripts.extend(sorted(system_packages_cmd))
    scripts.extend(p.section(section) for p in packages)
    return shell_out("\n".join(scripts), sudo=sudo, dry_run=dry_run)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--dry-run", action="store_true")
    parser.add_argument("--system",
                        choices=sorted(p.name
                                       for p in SystemMeta.known_systems),
                        default=CURRENT_SYSTEM.name)
    parser.add_argument("packages", nargs="*")
    return parser.parse_args()


def main():
    global CURRENT_SYSTEM

    opts = parse_args()

    if opts.system and opts.system != CURRENT_SYSTEM.name:
        for s in SystemMeta.known_systems:
            if s.name == opts.system:
                CURRENT_SYSTEM = s()
                break

    packages = all_packages()

    if opts.packages:
        packages = [p for p in packages if p.name in opts.packages]

    packages_run_section(packages, "install", sudo=True, dry_run=opts.dry_run)
    packages_run_section(packages, "setup", sudo=False, dry_run=opts.dry_run)


if __name__ == "__main__":
    main()
