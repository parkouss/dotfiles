(in-package :stumpwm)
;(ql:quickload "swank")

;(swank:create-server)

(run-shell-command "xss-lock -- xlock -mode blank &")

;; fix scrolling issue with gt3 apps such as firefox
(setf (getenv "GDK_CORE_DEVICE_EVENTS") "1")

(set-prefix-key (kbd "M-Tab"))

(setf *MOUSE-FOCUS-POLICY* :click)

(defcommand firefox () ()
  "Start Firefox or switch to it, if it is already running."
  (run-or-raise "firefox" '(:class "Firefox")))
(define-key *root-map* (kbd "F") "firefox")

(defcommand dmenu_run () ()
  "run or raise dmenu"
  (run-or-raise "dmenu_run -l 10" '(:class "dmenu")))
(define-key *root-map* (kbd "r") "dmenu_run")

(defcommand alacritty () ()
  "run or raise alacritty (terminal)"
  (run-or-raise "alacritty -e tmux" '(:class "Alacritty")))
(define-key *root-map* (kbd "t") "alacritty")

(define-key *root-map* (kbd "L") "exec xlock -mode blank")
(define-key *root-map* (kbd "@") "exec systemctl suspend")

(define-key *root-map* (kbd "eacute") "windowlist")
(define-key *root-map* (kbd "w") "windowlist")

(define-key *root-map* (kbd "b") "move-focus left")
(define-key *root-map* (kbd "f") "move-focus right")
(define-key *root-map* (kbd "p") "move-focus up")
(define-key *root-map* (kbd "n") "move-focus down")
(define-key *root-map* (kbd "M-b") "move-window left")
(define-key *root-map* (kbd "M-f") "move-window right")
(define-key *root-map* (kbd "M-p") "move-window up")
(define-key *root-map* (kbd "M-n") "move-window down")

;; remap keys to have emacs like bindings!
(define-remapped-keys '(("firefox"
                         ("C-n" . "Down")
                         ("C-p" . "Up")
                         ("C-g" . "ESC")
                         ("C-v" . "Page_Down")
                         ("M-v" . "Page_Up")
                         ("C-y" . "C-v")
                         ("M-w" . "C-c"))))
