#------------------------------
# History stuff
#------------------------------
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000

#------------------------------
# Keybindings
#------------------------------
bindkey -e # emacs bindings
# to split words a bit more like emacs does, for M-f and M-b navigation
WORDCHARS=""


#------------------------------
# Alias stuff
#------------------------------
alias ls="ls --color -F"
alias ll="ls --color -lh"

#------------------------------
# man, coloured manual
#------------------------------
man() {
    env \
        LESS_TERMCAP_mb=$(printf "\e[1;31m") \
        LESS_TERMCAP_md=$(printf "\e[1;31m") \
        LESS_TERMCAP_me=$(printf "\e[0m") \
        LESS_TERMCAP_se=$(printf "\e[0m") \
        LESS_TERMCAP_so=$(printf "\e[1;44;33m") \
        LESS_TERMCAP_ue=$(printf "\e[0m") \
        LESS_TERMCAP_us=$(printf "\e[1;32m") \
        man "$@"
}

#---------------------------
# Completion
#---------------------------
autoload -Uz compinit

compinit

#---------------------------
# Prompt
#---------------------------
autoload -Uz promptinit
promptinit

prompt pws

#---------------------------
# custom post init
#---------------------------
[ -f ~/.zshrc-post-init ] && source ~/.zshrc-post-init
